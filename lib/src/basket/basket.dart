import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import '/src/basket/element_info.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart'
    as scanner;

class Basket extends StatefulWidget {
  const Basket({Key? key}) : super(key: key);

  @override
  State<Basket> createState() => _BasketState();
}

class _BasketState extends State<Basket> {
  final List<LocalBasket> _basket = [];

  late Future<bool> _basketDetails;

  double totalPrice = 0;
  int _choix = 0;
  String _choixTxt = "Sur Place";

  Future<bool> _getBasket() async {
    Database database = await openDatabase(
        join(await getDatabasesPath(), "kmandi_database.db"));

    List<Map<String, dynamic>> maps = await database.query("basket");
    for (int i = 0; i < maps.length; i++) {
      _basket.add(LocalBasket(maps[i]["_id"], maps[i]["image"],
          double.parse(maps[i]["price"].toString()), maps[i]["name"]));
      totalPrice += double.parse(maps[i]["price"].toString());
    }
    return true;
  }

  @override
  void initState() {
    _basketDetails = _getBasket();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _basketDetails,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData) {
          return Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "Total : " + totalPrice.toString() + " TND",
                    textScaleFactor: 1.5,
                  )
                ],
              ),
              Container(
                  margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: const Divider(color: Colors.red)),
              Expanded(
                child: ListView.builder(
                  itemCount: _basket.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ElementInfo(_basket[index].id, _basket[index].image,
                        _basket[index].price, _basket[index].name);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [0, 1]
                          .map((int index) => Radio<int>(
                                value: index,
                                groupValue: _choix,
                                onChanged: (int? value) {
                                  if (value != null) {
                                    setState(() {
                                      _choix = value;
                                      _choix == 0
                                          ? _choixTxt = "Sur Place"
                                          : _choixTxt = "Livraison";
                                      _choixTxt;
                                    });
                                  }
                                },
                              ))
                          .toList(),
                    ),
                    Column(
                      children: const [
                        Text("Sur place"),
                        SizedBox(
                          height: 30,
                        ),
                        Text("Livraison"),
                      ],
                    ),
                    const Spacer(),
                    SizedBox(
                      height: 70,
                      width: 200,
                      child: ElevatedButton(
                        child: Text(_choixTxt),
                        style: Helper.kmandiButtonStyle(Helper.brownishColor),
                        onPressed: () {
                          if (_choix == 0) {
                            _scanQR();
                          } else {
                            Navigator.pushReplacementNamed(
                                context, "/MapSample");
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  String result = "";
  Future _scanQR() async {
    try {
      String? cameraScanResult =
          await scanner.FlutterBarcodeScanner.scanBarcode(
              "#ff6666", "Cancel", false, scanner.ScanMode.QR);
      setState(() {
        result =
            cameraScanResult; // setting string result with cameraScanResult
      });
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("table", result);
    } catch (err) {
      result = err.toString();
    }
  }
}
