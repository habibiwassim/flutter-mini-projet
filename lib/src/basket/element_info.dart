import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class ElementInfo extends StatefulWidget {
  final String _id;
  final String _image;
  final double _price;
  final String _name;

  ElementInfo(this._id, this._image, this._price, this._name);

  @override
  State<ElementInfo> createState() => _ElementInfoState();
}

class _ElementInfoState extends State<ElementInfo> {
  late Future<bool> _dbLoaded;
  late Database database;


  Future<bool> _getDb() async {
    database = await openDatabase(
        join(await getDatabasesPath(), "kmandi_database.db"));

    return true;
  }

  @override
  void initState() {
    _dbLoaded = _getDb();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _dbLoaded,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              height: 120,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Card(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      IconButton(
                          onPressed: () async {
                            await database.rawDelete(
                                'DELETE FROM basket WHERE _id = ?',
                                [widget._id]);
                          },
                          icon: const Icon(
                            Icons.delete,
                            size: 50,
                          )),
                      Container(
                        margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Image.network(
                            "http://10.0.2.2:4000/uploads/" + widget._image,
                            width: 155,
                            height: 58),
                      ),
                      Text(
                        widget._name + "   ",
                        textScaleFactor: 1.2,
                      ),
                      Text(
                        widget._price.toString() + " TND",
                        textScaleFactor: 1.2,
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}

class LocalBasket {
  final String id;
  final String image;
  final double price;
  final String name;

  LocalBasket(this.id, this.image, this.price, this.name);

  @override
  String toString() {
    return 'LocalBasket{id: $id, image: $image, price: $price,name: $name}';
  }
}
