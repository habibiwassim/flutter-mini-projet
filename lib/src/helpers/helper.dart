import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Helper {
  static const brownishColor = Color(0xFFB7573C);
  static const brownishDarkColor = Color(0xFF77240D);
  static const primaryColor = Color(0xFF272727);
  static const hintColor = Color(0xFF847F7E);

  static ButtonStyle kmandiButtonStyle(Color couleur) {
    return ButtonStyle(
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
              side: BorderSide(color: couleur))),
      backgroundColor: MaterialStateProperty.all<Color>(couleur),
    );
  }

  static String? passwordValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "Le mot de passe ne doit pas etre vide";
    } else if (value.length < 5) {
      return "Le mot de passe doit avoir au moins 5 caractères";
    } else {
      return null;
    }
  }

  static String? emailValidator(String? value) {
    String pattern =
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    if (value == null || value.isEmpty) {
      return "L'adresse email ne doit pas etre vide";
    } else if (!RegExp(pattern).hasMatch(value)) {
      return "L'adresse email est incorrecte";
    } else {
      return null;
    }
  }

  static String? confpwValidator(String? value, String? confValue) {
    if (value == null || value.isEmpty) {
      return "Le mot de passe ne doit pas etre vide";
    } else if (value.length < 5) {
      return "Le mot de passe doit avoir au moins 5 caractères";
    } else if (value != confValue) {
      return "Les deux champs ne sont pas conformes";
    } else {
      return null;
    }
  }

  static String? addressValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "L'adresse ne doit pas etre vide";
    } else {
      return null;
    }
  }

  static String? phoneValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "Le numéro de téléphone ne doit pas etre vide";
    } else if (value.length < 5) {
      return "Le numéro de téléphone doit avoir au moins 8 chiffres";
    } else {
      return null;
    }
  }

  static String? nameValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "Le Nom ne doit pas etre vide";
    } else if (value.length < 5) {
      return "Le Nom doit avoir au moins 5 caractères";
    } else {
      return null;
    }
  }

  static InputDecoration kmandiInputDecoration(String labeltxt) {
    return InputDecoration(
      labelText: labeltxt,
      labelStyle: GoogleFonts.roboto(
        color: Helper.hintColor,
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: const BorderSide(color: Helper.hintColor, width: 2),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Helper.brownishColor, width: 2),
        borderRadius: BorderRadius.circular(30),
      ),
    );
  }
}
