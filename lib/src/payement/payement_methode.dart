import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Payement extends StatelessWidget {
  const Payement({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Payement Option'),
        ),
        body: Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/background.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              children: <Widget>[
                FloatingActionButton.extended(onPressed: () {/*ToDo : cash payement */}, label: Text('Cash',),),
                FloatingActionButton.extended(onPressed: () {Navigator.pushReplacementNamed(context, "/CC");}, label: Text('CreditCard',),),
              ],
            )));
  }

}