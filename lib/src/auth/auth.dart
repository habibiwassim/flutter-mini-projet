import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Auth extends StatelessWidget {
  const Auth({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/background.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: ListView(
              children: [
                const SizedBox(
                  height: 100,
                ),
                Image.asset(
                  "assets/images/logoFlutter.png",
                ),
                const SizedBox(
                  height: 10,
                ),
                Text.rich(
                  TextSpan(
                    style: GoogleFonts.roboto(
                      fontSize: 24.0,
                      color: const Color(0xFFA5A5A5),
                      letterSpacing: 2.5,
                    ),
                    children: [
                      const TextSpan(
                        text: 'WEATHER GETS COLD\n',
                      ),
                      TextSpan(
                        text: 'OUR DRINKS DON\'T',
                        style: GoogleFonts.roboto(
                          color: const Color(0xFF707070),
                        ),
                      ),
                    ],
                  ),
                  textAlign: TextAlign.center,
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 80, 20, 00),
                  child: ElevatedButton(
                    child: SizedBox(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Login",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                              fontSize: 16.0,
                              color: Colors.white70,
                              letterSpacing: 1.6,
                            ),
                          ),
                        ],
                      ),
                      width: 300,
                      height: 50,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, "/Signin");
                    },
                    style: kmandiButtonStyle(),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 20, 20, 00),
                  child: ElevatedButton(
                    child: SizedBox(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Create an Account",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                              fontSize: 16.0,
                              color: const Color(0xFFb7573c),
                              letterSpacing: 1.6,
                            ),
                          ),
                        ],
                      ),
                      width: 300,
                      height: 50,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, "/Signup");
                    },
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              side: const BorderSide(
                                  color: Color(0xFFb7573c), width: 3))),
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Colors.white.withOpacity(0.5)),
                    ),
                  ),
                ),
              ],
            )));
  }

  ButtonStyle kmandiButtonStyle() {
    return ButtonStyle(
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
              side: const BorderSide(color: Color(0xFFb7573c)))),
      backgroundColor:
          MaterialStateProperty.all<Color>(const Color(0xFFb7573c)),
    );
  }
}
