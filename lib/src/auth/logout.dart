import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Logout extends StatelessWidget {
  const Logout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SizedBox(
          width: 412.0,
          height: 870.0,
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, "/");
                },
                child: Container(
                  alignment: Alignment.center,
                  width: 332.0,
                  height: 54.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(32.0),
                    color: const Color(0xFFB7573C),
                    border: Border.all(
                      width: 2.0,
                      color: const Color(0xFFB7573C),
                    ),
                  ),
                  child: Text(
                    'Sign Out',
                    style: GoogleFonts.roboto(
                      fontSize: 16.0,
                      color: Colors.white,
                      letterSpacing: 1.6,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
