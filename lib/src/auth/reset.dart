import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Reset extends StatefulWidget {
  const Reset({Key? key}) : super(key: key);

  @override
  State<Reset> createState() => _ResetState();
}

class _ResetState extends State<Reset> {
  late String? _email;
  late String? _id;
  late String? _password;
  late String? token;

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  final String _baseUrl = "10.0.2.2:4000";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
          key: _keyForm,
          child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/background.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: ListView(
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Reset Password',
                    style: GoogleFonts.roboto(
                      fontSize: 35.0,
                      color: Helper.primaryColor,
                      letterSpacing: 3.5,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Enter your email address',
                    style: GoogleFonts.roboto(
                      fontSize: 16.0,
                      color: Helper.primaryColor,
                      letterSpacing: 1.6,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                    child: TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: Helper.kmandiInputDecoration("Email Address"),
                      onSaved: (String? value) {
                        _email = value;
                      },
                      validator: (String? value) {
                        return Helper.emailValidator(value);
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 20),
                    child: ElevatedButton(
                      child: SizedBox(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Reset",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.roboto(
                                fontSize: 16.0,
                                color: Colors.white70,
                                letterSpacing: 1.6,
                              ),
                            ),
                          ],
                        ),
                        width: 300,
                        height: 50,
                      ),
                      onPressed: () async {
                        if (_keyForm.currentState!.validate()) {
                          _keyForm.currentState!.save();

                          Map<String, dynamic> userData = {
                            "email": _email,
                          };
                          Map<String, String> headers = {
                            "Content-Type": "application/json; charset=UTF-8"
                          };
                          http
                              .put(Uri.http(_baseUrl, "/users/reset/"+_email!),
                              headers: headers)
                              .then((http.Response response) async {
                            if (response.statusCode == 200) {
                                  print("passwored reseted");
                                  Navigator.pushReplacementNamed(context, "/Signin");
                                  showDialog(context: context, builder: (BuildContext context) {
                                    return const AlertDialog(
                                          title: Text("Informationn"),
                                           content: Text("Password reseted Successfuly !!"),
                                    );
                                });}
                              //Navigator.pushReplacementNamed(context, "/navBottom");

                            else {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return const AlertDialog(
                                      title: Text("Information"),
                                      content: Text(
                                          "Une erreur s'est produite, réessayer plus tard !"),
                                    );
                                  });
                            }
                          });
                        }
                        //Navigator.pushReplacementNamed(context, "/Slideone");
                      },
                      style: Helper.kmandiButtonStyle(Helper.brownishColor),
                    ),
                  ),
                  Text(
                    "Don't have an Account ?",
                    style: GoogleFonts.roboto(
                      fontSize: 16.0,
                      color: Helper.primaryColor,
                      letterSpacing: 1.1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  InkWell(
                    child: Text(
                      "Sign up",
                      style: GoogleFonts.roboto(
                        fontSize: 16.0,
                        color: Helper.brownishColor,
                        letterSpacing: 1.6,
                        decoration: TextDecoration.underline,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    onTap: () {
                      Navigator.pushReplacementNamed(context, "/Signup");
                    },
                  ),
                  const SizedBox(height: 50),
                  Text(
                    'or Login With',
                    style: GoogleFonts.roboto(
                      fontSize: 16.0,
                      color: Helper.primaryColor,
                      letterSpacing: 1.6,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                    child: ElevatedButton(
                      child: SizedBox(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.facebook_rounded),
                            const SizedBox(
                              width: 30,
                            ),
                            Text(
                              "Facebook",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.roboto(
                                fontSize: 16.0,
                                color: Colors.white70,
                                letterSpacing: 1.6,
                              ),
                            ),
                          ],
                        ),
                        width: 300,
                        height: 50,
                      ),
                      onPressed: () {
                        // Facebook Authentification API
                      },
                      style: Helper.kmandiButtonStyle(const Color(0xFF0f71b3)),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 20),
                    child: ElevatedButton(
                      child: SizedBox(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.account_circle),
                            const SizedBox(
                              width: 30,
                            ),
                            Text(
                              "Google",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.roboto(
                                fontSize: 16.0,
                                color: Colors.white70,
                                letterSpacing: 1.6,
                              ),
                            ),
                          ],
                        ),
                        width: 300,
                        height: 50,
                      ),
                      onPressed: () {
                        // Facebook Authentification API
                      },
                      style: Helper.kmandiButtonStyle(const Color(0xFFb30f0f)),
                    ),
                  ),

                ],
              ))),
    );
  }
}