import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class VerifPhone extends StatefulWidget {
  const VerifPhone({Key? key}) : super(key: key);

  @override
  State<VerifPhone> createState() => _VerifPhoneState();
}

class _VerifPhoneState extends State<VerifPhone> {
  late String? _verifCode;
  late String? _id;
  late String? _password;
  late String? token;

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  final String _baseUrl = "10.0.2.2:4000";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
          key: _keyForm,
          child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/background.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: ListView(
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Verify Your Number',
                    style: GoogleFonts.roboto(
                      fontSize: 35.0,
                      color: Helper.primaryColor,
                      letterSpacing: 3.5,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Enter the Code sent Via SMS',
                    style: GoogleFonts.roboto(
                      fontSize: 16.0,
                      color: Helper.primaryColor,
                      letterSpacing: 1.6,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                    child: TextFormField(
                      decoration: Helper.kmandiInputDecoration("SMS Code"),
                      onSaved: (String? value) {
                        _verifCode = value;
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 20),
                    child: ElevatedButton(
                      child: SizedBox(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Verify PhoneNumber",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.roboto(
                                fontSize: 16.0,
                                color: Colors.white70,
                                letterSpacing: 1.6,
                              ),
                            ),
                          ],
                        ),
                        width: 300,
                        height: 50,
                      ),
                      onPressed: () async {
                        if (_keyForm.currentState!.validate()) {
                          _keyForm.currentState!.save();
                          SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                           token = prefs.getString("token");
                           var number = prefs.getString("number");
                          Map<String, dynamic> userData = {
                            "verifCode": _verifCode,
                          };
                          Map<String, String> headers = {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                            'Authorization': 'Bearer $token',
                          };
                          print("verifcode : "+ _verifCode!);
                          http
                              .put(Uri.http(_baseUrl, "/users/verif-number/"+number!+"/"+_verifCode!),
                              headers: headers)
                              .then((http.Response response) async {
                                print(response.toString());
                            if (response.statusCode == 200) {
                              print("phone verified");
                              Navigator.pushReplacementNamed(context, "/Slideone");
                              showDialog(context: context, builder: (BuildContext context) {
                                return const AlertDialog(
                                  title: Text("Informationn"),
                                  content: Text("Phone Number Verified !!"),
                                );
                              });}
                            //Navigator.pushReplacementNamed(context, "/navBottom");

                            else {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return const AlertDialog(
                                      title: Text("Information"),
                                      content: Text(
                                          "Une erreur s'est produite, réessayer plus tard !"),
                                    );
                                  });
                            }
                          });
                        }
                        //Navigator.pushReplacementNamed(context, "/Slideone");
                      },
                      style: Helper.kmandiButtonStyle(Helper.brownishColor),
                    ),
                  ),
                  Text(
                    "SMS not recieved ?",
                    style: GoogleFonts.roboto(
                      fontSize: 16.0,
                      color: Helper.primaryColor,
                      letterSpacing: 1.1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  InkWell(
                    child: Text(
                      "Resend Code",
                      style: GoogleFonts.roboto(
                        fontSize: 16.0,
                        color: Helper.brownishColor,
                        letterSpacing: 1.6,
                        decoration: TextDecoration.underline,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    onTap: () async{
                      SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                      token = prefs.getString("token");
                      var number = prefs.getString("number");

                      Map<String, String> headers = {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer $token',
                      };
                      http.put(Uri.http(_baseUrl, "/users/send-phone-verification/"+number!),headers: headers)
                      .then((http.Response response) async {
                        if (response.statusCode == 200) {
                          print("SMS Sent");
                          showDialog(context: context, builder: (BuildContext context) {
                            return const AlertDialog(
                              title: Text("Informationn"),
                              content: Text("SMS Sent  !!"),
                            );
                          });}
                        else {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return const AlertDialog(
                                  title: Text("Information"),
                                  content: Text(
                                      "Une erreur s'est produite, réessayer plus tard !"),
                                );
                              });
                        }
                      });
                    },
                  ),
                ],
              ))),
    );
  }
}