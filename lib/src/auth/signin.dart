import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class Signin extends StatefulWidget {
  const Signin({Key? key}) : super(key: key);

  @override
  State<Signin> createState() => _SigninState();
}

class _SigninState extends State<Signin> {
  late String? _email;
  late String? _password;
  late String? token;

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  final String _baseUrl = "10.0.2.2:4000";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
          key: _keyForm,
          child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/background.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: ListView(
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Login',
                    style: GoogleFonts.roboto(
                      fontSize: 35.0,
                      color: Helper.primaryColor,
                      letterSpacing: 3.5,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Add your details to login',
                    style: GoogleFonts.roboto(
                      fontSize: 16.0,
                      color: Helper.primaryColor,
                      letterSpacing: 1.6,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                    child: TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: Helper.kmandiInputDecoration("Email Address"),
                      onSaved: (String? value) {
                        _email = value;
                      },
                      validator: (String? value) {
                        return Helper.emailValidator(value);
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 10),
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      obscureText: true,
                      decoration: Helper.kmandiInputDecoration("Mot de passe"),
                      onSaved: (String? value) {
                        _password = value;
                      },
                      validator: (String? value) {
                        return Helper.passwordValidator(value);
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 20),
                    child: ElevatedButton(
                      child: SizedBox(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Login",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.roboto(
                                fontSize: 16.0,
                                color: Colors.white70,
                                letterSpacing: 1.6,
                              ),
                            ),
                          ],
                        ),
                        width: 300,
                        height: 50,
                      ),
                      onPressed: () {
                        if (_keyForm.currentState!.validate()) {
                          _keyForm.currentState!.save();
                          Map<String, dynamic> userData = {
                            "email": _email,
                            "password": _password
                          };

                          Map<String, String> headers = {
                            "Content-Type": "application/json; charset=UTF-8"
                          };

                          http
                              .post(Uri.http(_baseUrl, "/users/authenticate"),
                                  headers: headers, body: json.encode(userData))
                              .then((http.Response response) async {
                            Database database = await openDatabase(
                                join(await getDatabasesPath(),
                                    "kmandi_database.db"),
                                onCreate: (Database db, int version) {
                              db.transaction((Transaction txn) async {
                                await txn.execute(
                                    "CREATE TABLE basket(_id TEXT PRIMARY KEY, image TEXT, price FLOAT, name TEXT, token TEXT)");
                              });
                            }, version: 1);
                            debugPrint(response.body.toString());
                            Map<String, dynamic> userFromServer =
                                json.decode(response.body);

                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            prefs.setString("token", userFromServer["token"]);
                            print(prefs.getString("token"));
                            if (response.statusCode == 200) {
                              prefs.setString(
                                  "name", userFromServer["fullName"]);
                              prefs.setString("id", userFromServer["id"]);
                              prefs.setBool(
                                  "isAdmin", userFromServer["isAdmin"]);
                              prefs.setString(
                                  "number", userFromServer["number"]);
                              debugPrint(prefs.getString("token").toString());
                              if (response.statusCode == 200) {
                                //Navigator.pushReplacementNamed(context, "/navBottom");
                                //Navigator.pushReplacementNamed(context, "/Slideone");
                                if (userFromServer["isVerified"]) {
                                  Navigator.pushReplacementNamed(
                                      context, "/Slideone");
                                } else {
                                  http.put(
                                      Uri.http(
                                          _baseUrl,
                                          "/users/send-phone-verification/" +
                                              userFromServer["number"]!),
                                      headers: {
                                        'Content-Type': 'application/json',
                                        'Accept': 'application/json',
                                        'Authorization':
                                            'Bearer ${userFromServer["token"]}',
                                      });
                                  Navigator.pushReplacementNamed(
                                      context, "/VerifPhone");
                                }
                              } else if (response.statusCode == 400) {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("Error"),
                                        content: Text(
                                            "Password et/ou email est incorrect !!"),
                                      );
                                    });
                              } else {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("Informationn"),
                                        content: Text(
                                            "error , ressayer plus tard !!"),
                                      );
                                    });
                              }
                            }
                          });
                        }
                        //Navigator.pushReplacementNamed(context, "/Slideone");
                      },
                      style: Helper.kmandiButtonStyle(Helper.brownishColor),
                    ),
                  ),
                  Text(
                    "Forgot your password ?",
                    style: GoogleFonts.roboto(
                      fontSize: 16.0,
                      color: Helper.primaryColor,
                      letterSpacing: 1.1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  InkWell(
                    child: Text(
                      "Reset it",
                      style: GoogleFonts.roboto(
                        fontSize: 16.0,
                        color: Helper.brownishColor,
                        letterSpacing: 1.6,
                        decoration: TextDecoration.underline,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    onTap: () {
                      Navigator.pushReplacementNamed(context, "/Reset");
                    },
                  ),
                  const SizedBox(height: 50),
                  Text(
                    'or Login With',
                    style: GoogleFonts.roboto(
                      fontSize: 16.0,
                      color: Helper.primaryColor,
                      letterSpacing: 1.6,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                    child: ElevatedButton(
                      child: SizedBox(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.facebook_rounded),
                            const SizedBox(
                              width: 30,
                            ),
                            Text(
                              "Facebook",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.roboto(
                                fontSize: 16.0,
                                color: Colors.white70,
                                letterSpacing: 1.6,
                              ),
                            ),
                          ],
                        ),
                        width: 300,
                        height: 50,
                      ),
                      onPressed: () {
                        // Facebook Authentification API
                      },
                      style: Helper.kmandiButtonStyle(const Color(0xFF0f71b3)),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 20),
                    child: ElevatedButton(
                      child: SizedBox(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.account_circle),
                            const SizedBox(
                              width: 30,
                            ),
                            Text(
                              "Google",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.roboto(
                                fontSize: 16.0,
                                color: Colors.white70,
                                letterSpacing: 1.6,
                              ),
                            ),
                          ],
                        ),
                        width: 300,
                        height: 50,
                      ),
                      onPressed: () {
                        // Facebook Authentification API
                      },
                      style: Helper.kmandiButtonStyle(const Color(0xFFb30f0f)),
                    ),
                  ),
                  Text(
                    "Don't have an Account ?",
                    style: GoogleFonts.roboto(
                      fontSize: 16.0,
                      color: Helper.primaryColor,
                      letterSpacing: 1.1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  InkWell(
                    child: Text(
                      "Sign up",
                      style: GoogleFonts.roboto(
                        fontSize: 16.0,
                        color: Helper.brownishColor,
                        letterSpacing: 1.6,
                        decoration: TextDecoration.underline,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    onTap: () {
                      Navigator.pushReplacementNamed(context, "/Signup");
                    },
                  ),
                ],
              ))),
    );
  }
}
