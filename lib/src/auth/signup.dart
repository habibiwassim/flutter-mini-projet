import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Signup extends StatefulWidget {
  const Signup({Key? key}) : super(key: key);

  @override
  State<Signup> createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  late String? _email;
  late String? _username;
  late String? _phonenumber;
  late String? _address;
  //late String? _password;
  //late String? _confirmPassword;
  TextEditingController _pass = TextEditingController();
  TextEditingController _confpass = TextEditingController();

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  final String _baseUrl = "10.0.2.2:4000";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Form(
            key: _keyForm,
            child: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/background.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: ListView(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Sign Up',
                      style: GoogleFonts.roboto(
                        fontSize: 35.0,
                        color: Helper.primaryColor,
                        letterSpacing: 3.5,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Add your details to Signup',
                      style: GoogleFonts.roboto(
                        fontSize: 16.0,
                        color: Helper.primaryColor,
                        letterSpacing: 1.6,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        decoration:
                            Helper.kmandiInputDecoration("Email Address"),
                        onSaved: (String? value) {
                          _email = value;
                        },
                        validator: (String? value) {
                          return Helper.emailValidator(value);
                        },
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                      child: TextFormField(
                        keyboardType: TextInputType.name,
                        decoration: Helper.kmandiInputDecoration("Name"),
                        onSaved: (String? value) {
                          _username = value;
                        },
                        validator: (String? value) {
                          return Helper.nameValidator(value);
                        },
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                      child: TextFormField(
                        keyboardType: TextInputType.phone,
                        decoration: Helper.kmandiInputDecoration("Phone N°"),
                        onSaved: (String? value) {
                          _phonenumber = value;
                        },
                        validator: (String? value) {
                          return Helper.phoneValidator(value);
                        },
                      ),
                    ),
                    Container(
                        margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                        child: TextFormField(
                          keyboardType: TextInputType.streetAddress,
                          decoration: Helper.kmandiInputDecoration("Address"),
                          maxLines: 3,
                          onSaved: (String? value) {
                            _address = value;
                          },
                          validator: (value) {
                            return Helper.addressValidator(value);
                          },
                        )),
                    Container(
                      margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        decoration:
                            Helper.kmandiInputDecoration("Mot de passe"),
                        //onSaved: (String? value) {
                        //  _password = value;
                        //},
                        controller: _pass,
                        validator: (String? value) {
                          return Helper.passwordValidator(value);
                        },
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(20, 30, 20, 10),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        decoration: Helper.kmandiInputDecoration(
                            "Confirmation de mot de passe"),
                        //onSaved: (String? value) {
                        //  _confirmPassword = value;
                        //},
                        controller: _confpass,
                        validator: (String? value) {
                          return Helper.confpwValidator(value, _pass.text);
                        },
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(20, 30, 20, 20),
                      child: ElevatedButton(
                        child: SizedBox(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Sign Up",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                  fontSize: 16.0,
                                  color: Colors.white70,
                                  letterSpacing: 1.6,
                                ),
                              ),
                            ],
                          ),
                          width: 300,
                          height: 50,
                        ),
                        onPressed: () {
                          if (_keyForm.currentState!.validate()) {
                            _keyForm.currentState!.save();
                            Map<String, dynamic> userData = {
                              "fullName": _username,
                              "email": _email,
                              "password": _pass.text,
                              "address": _address,
                              "number": _phonenumber
                            };
                            Map<String, String> headers = {
                              "Content-Type": "application/json; charset=UTF-8"
                            };
                            http
                                .post(Uri.http(_baseUrl, "/users/register"),
                                    headers: headers,
                                    body: json.encode(userData))
                                .then((http.Response response) async {
                              print(response.statusCode);
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                              if (response.statusCode == 200) {
                                Map<String, dynamic> userFromServer =
                                    json.decode(response.body);
                                prefs.setString(
                                    "token", userFromServer["token"]);
                                print(prefs.getString("token"));
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("Informationn"),
                                        content: Text(
                                            "Account Created Successfuly !!"),
                                      );
                                    });
                                //Navigator.pushNamed(context, "/Signin");
                              } else {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("Informationn"),
                                        content: Text(
                                            "Une erreur s'est produite, réessayer plus tard !"),
                                      );
                                    });
                              }
                            });
                          }
                          //Navigator.pushReplacementNamed(context, "/Signin");
                        },
                        style: Helper.kmandiButtonStyle(Helper.brownishColor),
                      ),
                    ),
                    Text(
                      "Already have an Account ?",
                      style: GoogleFonts.roboto(
                        fontSize: 16.0,
                        color: Helper.primaryColor,
                        letterSpacing: 1.1,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    InkWell(
                      child: Text(
                        "Login",
                        style: GoogleFonts.roboto(
                          fontSize: 16.0,
                          color: Helper.brownishColor,
                          letterSpacing: 1.6,
                          decoration: TextDecoration.underline,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      onTap: () {
                        Navigator.pushReplacementNamed(context, "/Signin");
                      },
                    ),
                    const SizedBox(height: 20)
                  ],
                ))));
  }
}
