import 'package:bottom_bar/bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/basket/basket.dart';
import 'package:flutter_mini_proj/src/events/event.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:flutter_mini_proj/src/home/home.dart';
import 'package:flutter_mini_proj/src/navigation/settings.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  State<BottomNav> createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  int _selectedIndex = 0;
  bool? role = false;
  final _pageController = PageController();
  static const _kPages = <String, IconData>{
    'Home': Icons.home,
    'Cart': Icons.shopping_bag,
    'Events': Icons.aspect_ratio,
    'Settings': Icons.settings
  };
  static const List _widgetOptions = [
    Home(),
    Basket(),
    Events(),
    Settings(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: PageView(
          controller: _pageController,
          children: [
            // _widgetOptions.elementAt(0),
            // _widgetOptions.elementAt(1),
            // _widgetOptions.elementAt(2),
            for (var widget in _widgetOptions) widget,
          ],
          onPageChanged: (index) {
            _onItemTapped(index);
            debugPrint("onPageChanged " + index.toString());
          },
        ),
      ),
      bottomNavigationBar: Theme(
          data: ThemeData(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
          ),
          child: BottomBar(
            selectedIndex: _selectedIndex,
            items: [
              for (final entry in _kPages.entries)
                BottomBarItem(
                  icon: Icon(entry.value),
                  title: Text(entry.key),
                  activeColor: Helper.brownishColor,
                  inactiveColor: Helper.hintColor,
                ),
            ],
            onTap: (int index) {
              _pageController.jumpToPage(index);
              _onItemTapped(index);
              debugPrint("onTap " + index.toString());
            },
          )),
    );
  }
}
