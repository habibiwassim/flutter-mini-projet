import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);
  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  bool role = false;

  @override
  void initState() {
    getRole().then((value) {
      if(value){
        setState(() {
          role = true;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                icon: const Icon(Icons.shopping_cart_outlined),
                color: Helper.brownishColor,
                onPressed: () {/*TODO navigation au panier*/},
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            child: CircleAvatar(
              child: Image.asset("assets/images/logoFlutter.png"),
            ),
            height: 130,
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            "Email Address",
            style: GoogleFonts.roboto(
              fontSize: 18.0,
              color: Helper.primaryColor,
              letterSpacing: 1.6,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 20,
          ),
          ListTile(
            title: const Text(
              "Edit Profile",
            ),
            subtitle: const Text(
              "Update your infos",
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.pushNamed(context, "/Profile");
            },
          ),
          ListTile(
            title: const Text(
              "Terms, Privacy & Policy",
            ),
            subtitle: const Text(
              "Things you may want to know",
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {},
          ),
          ListTile(
            title: const Text(
              "Contact Us",
            ),
            subtitle: const Text(
              "Email us your if you have an inquery or Call us anytime",
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {},
          ),

          Container(
            margin: const EdgeInsets.fromLTRB(20, 80, 20, 00),
            child: ElevatedButton(
              child: SizedBox(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Disconnect",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                        fontSize: 16.0,
                        color: Colors.white70,
                        letterSpacing: 1.6,
                      ),
                    ),
                  ],
                ),
                width: 300,
                height: 50,
              ),
              onPressed: () {
                Navigator.pushReplacementNamed(context, "/");
              },
              style: Helper.kmandiButtonStyle(Helper.brownishColor),
            ),
          ),
          Visibility(
            visible: role,
            child: Container(
              margin: const EdgeInsets.fromLTRB(20, 20, 20, 00),
              child: ElevatedButton(
                child: SizedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Admin Dashboard",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                          fontSize: 16.0,
                          color: Colors.white70,
                          letterSpacing: 1.6,
                        ),
                      ),
                    ],
                  ),
                  width: 300,
                  height: 50,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, "/Admin");
                },
                style: Helper.kmandiButtonStyle(Helper.brownishColor),
              ),
            ),
          ),
        ],
      ),
    );
  }
  Future<bool> getRole() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? isAdmin = await prefs.getBool("isAdmin");
    if(isAdmin==true){
      return true;
    }
    return false;
  }
}
