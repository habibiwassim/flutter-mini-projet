import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/basket/basket.dart';
import 'package:flutter_mini_proj/src/events/event.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:flutter_mini_proj/src/home/home.dart';
import 'package:flutter_mini_proj/src/navigation/settings.dart';
import 'package:flutter_mini_proj/src/user/profile.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

const _kPages = <String, IconData>{
  'Tracker': FontAwesomeIcons.map,
  'Favorites': FontAwesomeIcons.star,
  'Home': FontAwesomeIcons.home,
  'Profil': FontAwesomeIcons.userEdit,
  'Settings': Icons.settings_outlined
};

const List<Widget> _interfaces = [
  Events(),
  Basket(),
  Home(),
  Profile(),
  Settings(),
];

class BottomNav extends StatefulWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  final TabStyle _tabStyle = TabStyle.flip;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _interfaces.length,
      initialIndex: 2,
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
              child: TabBarView(
                children: [
                  for (final interface in _interfaces) interface,
                ],
              ),
            ),
          ],
        ),
        bottomNavigationBar: ConvexAppBar.badge(
          // Optional badge argument: keys are tab indices, values can be
          // String, IconData, Color or Widget.
          /*badge=*/ const <int, dynamic>{/*3: '99+'*/},
          style: _tabStyle,
          backgroundColor: Helper.brownishColor,
          color: Colors.white60,
          activeColor: Colors.white70,
          items: <TabItem>[
            for (final entry in _kPages.entries)
              TabItem(icon: entry.value, title: entry.key),
          ],
          onTap: (int i) => debugPrint('click index=$i'),
        ),
      ),
    );
  }
}
