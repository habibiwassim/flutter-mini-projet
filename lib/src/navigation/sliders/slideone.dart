import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Slideone extends StatefulWidget {
  const Slideone({Key? key}) : super(key: key);

  @override
  State<Slideone> createState() => _SlideoneState();
}

class _SlideoneState extends State<Slideone> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SizedBox(
          width: 412.0,
          height: 870.0,
          child: Stack(
            children: <Widget>[
              Positioned(
                bottom: 185.0,
                child: SizedBox(
                  width: 412.0,
                  height: 870.0,
                  child: Column(
                    children: <Widget>[
                      Spacer(flex: 206),
                      Container(
                        width: 412.0,
                        height: 153.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("assets/images/drinks.png"),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Spacer(flex: 72),
                      SizedBox(
                        width: 34.0,
                        height: 8.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: 8.0,
                              height: 8.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: const Color(0xFFFC6011),
                              ),
                            ),
                            Container(
                              width: 8.0,
                              height: 8.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.grey[350],
                              ),
                            ),
                            Container(
                              width: 8.0,
                              height: 8.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.grey[350],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Spacer(flex: 39),
                      Text(
                        'Find the Product that suits you',
                        style: GoogleFonts.roboto(
                          fontSize: 24.0,
                          color: const Color(0xFF272727),
                          letterSpacing: 2.4000000000000004,
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Spacer(flex: 39),
                      Text(
                        'Browse between a variety of drinks\nand mini-treats',
                        style: GoogleFonts.roboto(
                          fontSize: 19.0,
                          color: const Color(0xFF272727),
                          letterSpacing: 1.9000000000000001,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Spacer(flex: 39),
                      InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, "/Slidetwo");
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: 332.0,
                          height: 54.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(32.0),
                            color: const Color(0xFFB7573C),
                            border: Border.all(
                              width: 2.0,
                              color: const Color(0xFFB7573C),
                            ),
                          ),
                          child: Text(
                            'Next',
                            style: GoogleFonts.roboto(
                              fontSize: 16.0,
                              color: Colors.white,
                              letterSpacing: 1.6,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      Spacer(flex: 185),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
