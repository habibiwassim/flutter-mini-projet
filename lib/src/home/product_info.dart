import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';

import 'product_details.dart';

class ProductInfo extends StatelessWidget {
  final String _id;
  final String _image;
  final String _name;
  //final String _reviews;
  final String _category;
  final double _price;
  final String _token;
  final double _scale;
  ProductInfo(this._id, this._image, this._name, this._category, this._price,
      this._token, this._scale);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return ProductDetails(
                _id, _image, _name, _category, _price, _token);
          }));
        },
        child: productCard(context, _scale));
  }

  Container productCard(BuildContext context, double sc) {
    return Container(
        margin: const EdgeInsets.all(5),
        child: Center(
          child: Column(
            children: [
              Transform.scale(
                  scale: sc,
                  child: Container(
                    width: 150,
                    height: 100,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    clipBehavior: Clip.antiAlias,
                    child: Image.network(
                      "http://10.0.2.2:4000/uploads/" + _image,
                      headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer $_token',
                      },
                      fit: BoxFit.fitWidth,
                    ),
                  )),
              const SizedBox(
                height: 10,
              ),
              Text(
                _name,
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: Helper.primaryColor, fontSize: 16),
              ),
              const SizedBox(
                height: 5,
              )
            ],
          ),
        ));
  }
}

class Product {
  final String id;
  final String image;
  final String name;
  //final String reviews;
  final String category;
  final double price;
  final String token;

  Product(
      this.id, this.image, this.name, this.category, this.price, this.token);

  @override
  String toString() {
    return 'Product{id: $id, image: $image, name: $name, category: $category,price: $price}';
  }
}
