import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:flutter_mini_proj/src/home/reviews.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:readmore/readmore.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class ProductDetails extends StatefulWidget {
  const ProductDetails(this._id, this._image, this._name, this._category,
      this._price, this._token,
      {Key? key})
      : super(key: key);

  //final String _description;
  final String _category;
  final String _id;
  final String _image;
  final String _name;
  final double _price;
  final String _token;

  @override
  State<ProductDetails> createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  late bool isLiked = false;
  int count = 0;

  final String _baseUrl = "10.0.2.2:4000";
  @override
  void initState() {
    super.initState();
  }

  void like() {
    setState(() {
      isLiked = !isLiked;
      if (!isLiked) {
        count--;
      } else {
        count++;
      }
      debugPrint(count.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(widget._name),
          backgroundColor: Colors.white.withOpacity(0),
          foregroundColor: Helper.brownishColor,
          shadowColor: Colors.white.withOpacity(0),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back_ios),
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.shopping_cart_outlined),
              onPressed: () {/*TODO navigation au panier*/},
            ),
          ],
        ),
        body: ListView(
          children: [
            Container(
              width: double.infinity,
              margin: const EdgeInsets.all(20),
              child: Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    clipBehavior: Clip.antiAlias,
                    child: Image.network(
                      "http://10.0.2.2:4000/uploads/" + widget._image,
                      headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ${widget._token}',
                      },
                    ),
                  ),
                  Container(
                    //margin: const EdgeInsets.all(20),
                    //padding: const EdgeInsets.all(20),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.vertical(
                        bottom: Radius.circular(20),
                      ),
                      //color: Colors.black.withOpacity(0.5),
                    ),
                    clipBehavior: Clip.antiAlias,
                    height: 100,
                    width: double.infinity,
                    child: ImageFiltered(
                      imageFilter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            gradient: LinearGradient(
                              begin: FractionalOffset.topCenter,
                              end: FractionalOffset.bottomCenter,
                              colors: [
                                Colors.grey.withOpacity(0.0),
                                Colors.black,
                              ],
                              stops: const [0.0, 1.0],
                            )),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: ListTile(
                        dense: true,
                        title: Text(
                          widget._name,
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 22.0),
                        ),
                        subtitle: Text(
                          widget._name,
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 16.0),
                        ),
                        trailing: InkWell(
                          child: Transform.scale(
                              child: isLiked
                                  ? const Icon(
                                      Icons.favorite,
                                      color: Colors.red,
                                    )
                                  : const Icon(
                                      Icons.favorite_border,
                                      color: Colors.white,
                                    ),
                              scale: 2),
                          onTap: () {
                            like();
                          },
                        )
                        //tileColor: Colors.black,
                        ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(20),
              child: SizedBox(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "Description :",
                        textScaleFactor: 1.2,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      ReadMoreText(
                        "Flutter is Google’s mobile UI open source framework"
                        "to build high-quality native (super fast) interfaces "
                        "for iOS and Android apps with the unified codebase."
                        "Flutter is Google’s mobile UI open source framework"
                        "to build high-quality native (super fast) interfaces "
                        "for iOS and Android apps with the unified codebase."
                        "Flutter is Google’s mobile UI open source framework"
                        "to build high-quality native (super fast) interfaces "
                        "for iOS and Android apps with the unified codebase.",
                        trimLines: 2,
                        colorClickableText: Helper.brownishColor,
                        trimMode: TrimMode.Line,
                        trimCollapsedText: 'Show more',
                        trimExpandedText: 'Show less',
                        moreStyle: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Helper.brownishColor),
                        lessStyle: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Helper.brownishColor),
                        style: TextStyle(color: Helper.primaryColor),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "Prix: " + widget._price.toString() + " TND",
                  textScaleFactor: 2,
                  textAlign: TextAlign.left,
                ),
              ],
            ), //Text("Category : " + widget._category.toString()),
          ],
        ),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext context) {
                  return Reviews(widget._name,widget._id);
                }));
              },
              child: Text(
                "Reviews",
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(
                  fontSize: 18.0,
                  color: Helper.brownishColor,
                  letterSpacing: 1.6,
                ),
              ),
              style:
                  Helper.kmandiButtonStyle(Helper.hintColor.withOpacity(0.1)),
            ),
            ElevatedButton(
              onPressed: () {
                http.get(
                  Uri.http(_baseUrl, "/products/" + widget._id),
                  headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ${widget._token}',
                  },
                ).then((http.Response response) async {
                  Database database = await openDatabase(
                      join(await getDatabasesPath(), "kmandi_database.db"));

                  Map<String, dynamic> product = {
                    "_id": widget._id,
                    "image": widget._image,
                    "price": widget._price,
                    "name": widget._name,
                    "token": widget._token
                  };

                  await database.insert("basket", product,
                      conflictAlgorithm: ConflictAlgorithm.replace);

                  List<Map<String, dynamic>> maps =
                      await database.query("basket");
                  print(maps);
                  Navigator.pop(context);
                });
              },
              child: Text(
                "Add to Basket",
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(
                  fontSize: 18.0,
                  color: Colors.white70,
                  letterSpacing: 1.6,
                ),
              ),
              style: Helper.kmandiButtonStyle(Helper.brownishColor),
            ),
          ],
        ));
  }
}
