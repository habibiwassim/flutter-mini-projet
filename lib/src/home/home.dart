import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter_mini_proj/src/home/product_info.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late Future<bool> fetchedProducts;
  late String token;
  var name;
  late Future<bool> fetchedname;
  final String _baseUrl = "10.0.2.2:4000";
  final List<Product> _products = [];

  @override
  void initState() {
    fetchedProducts = fetchProducts();
    fetchedname = fetchname();
    super.initState();
  }

  Future<bool> fetchProducts() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token = prefs.getString("token").toString();
    name = prefs.getString("name").toString();
    http.Response response =
        await http.get(Uri.http(_baseUrl, "/products"), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    debugPrint(response.body.toString());
    List<dynamic> productsFromServer = json.decode(response.body);
    debugPrint(productsFromServer[0].toString());
    debugPrint(productsFromServer[0]["category"]["name"].toString());
    // ignore: unused_local_variable
    var image = productsFromServer[0]["image"];

    debugPrint(
        double.parse(productsFromServer[0]["price"].toString()).toString());
    for (int i = 0; i < productsFromServer.length; i++) {
      Map<String, dynamic> productFromServer = productsFromServer[i];
      List<String> imageName = productFromServer["image"].split("\\");
      debugPrint(productFromServer["_id"].toString());
      _products.add(Product(
          productFromServer["_id"],
          imageName[1],
          productFromServer["name"],
          productFromServer["category"]["name"],
          double.parse(productFromServer["price"].toString()),
          token));
    }
    return true;
  }

  Future<bool> fetchname() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    name = prefs.getString("name").toString();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            ListTile(
              contentPadding: const EdgeInsets.fromLTRB(10, 30, 10, 10),
              leading: Text(
                "What you'd like to drink today? ",
                style: Theme.of(context).textTheme.headline5,
              ),
            ),
            TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                prefixIcon: const Icon(Icons.search),
                labelText: "Search Coffee",
                labelStyle: const TextStyle(
                  color: Colors.brown,
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: const BorderSide(color: Colors.brown, width: 2),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.brown, width: 2),
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              "Categories",
              style: Theme.of(context)
                  .textTheme
                  .headline6!
                  .copyWith(color: Helper.primaryColor, fontSize: 16),
            ),
            Container(
              height: 150,
              child: getCategories(),
            ),
            const Divider(),
            Text(
              "Recommanded Coffee",
              style: Theme.of(context)
                  .textTheme
                  .headline6!
                  .copyWith(color: Helper.primaryColor, fontSize: 16),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 80),
              child: getProducts(),
            ),
          ],
        ),
      ),
    );
  }

  ListView horizontalList(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      physics: const BouncingScrollPhysics(),
      itemCount: _products.length,
      itemBuilder: (BuildContext context, int index) => ProductInfo(
          _products[index].id,
          _products[index].image,
          _products[index].name,
          _products[index].category,
          _products[index].price,
          _products[index].token,
          0.6),
    );
  }

  ListView verticalList(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: _products.length,
      itemBuilder: (BuildContext context, int index) => ProductInfo(
          _products[index].id,
          _products[index].image,
          _products[index].name,
          _products[index].category,
          _products[index].price,
          _products[index].token,
          1.2),
    );
  }

  FutureBuilder<bool> getCategories() {
    return FutureBuilder(
      future: fetchedProducts,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData) {
          return horizontalList(context);
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  FutureBuilder<bool> getProducts() {
    return FutureBuilder(
      future: fetchedProducts,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData) {
          return verticalList(context);
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
