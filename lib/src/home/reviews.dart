import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:readmore/readmore.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Reviews extends StatefulWidget {
  final String _productName;
  final String _productId;
  const Reviews(
      this._productName,
      this._productId, {
        Key? key,
      }) : super(key: key);

  @override
  State<Reviews> createState() => _ReviewsState();
}

class _ReviewsState extends State<Reviews> {
  late Future<bool> fetchedReviews;
  final List<Review> _reviews = [];
  int numberLikes = 0;
  final String _baseUrl = "10.0.2.2:4000";
  late String token;
  late String comment = "dfqdfqdsfqdskfqs";

  @override
  void initState() {
    fetchedReviews = fetchReviews();
    super.initState();
  }

  Future<bool> fetchReviews() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    token = prefs.getString("token").toString();
    prefs.setString("productId",widget._productId);
    http.Response response =
    await http.get(Uri.http(_baseUrl, "/products/"+widget._productId), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${token}',
    });
    debugPrint(response.body.toString());
    Map<String, dynamic> map = json.decode(response.body);
    List<dynamic> data = map["reviews"];
    debugPrint(data[0]["Description"]);
    print(data[0]["User"]);
    print(data.length);
    for (int i = 0; i < data.length; i++) {
      var userId = data[i]["User"];
      var comment = data[i]["Description"];
      var createdDate = data[i]["createdDate"];
      //var rating = int.parse(data[i]["Rating"].toString());
      debugPrint(comment.toString());
      //debugPrint(int.parse(data[i]["Rating"].toString()).toString());
      http.Response userRes  = await http.get(Uri.http(_baseUrl, "/users/"+ userId), headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${token}',
      });
      Map<String, dynamic> userFromServer =
      json.decode(userRes.body);
      var username = userFromServer["fullName"];
      var userAvatar = userFromServer["avatar"];
      _reviews
          .add(Review(username, userAvatar, createdDate.toString(), comment));

  }
  return true;
}

final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
@override
Widget build(BuildContext context) {
  return Scaffold(
      appBar: AppBar(
        centerTitle: true,

        title: Text(
          widget._productName + " reviews",
        ),

        backgroundColor: Colors.white.withOpacity(0),
        foregroundColor: Helper.brownishColor,
        shadowColor: Colors.white.withOpacity(0),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back_ios),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.shopping_cart_outlined),

            onPressed: () async{/*TODO navigation au panier*/},
          ),

        ],
      ),
      body: ListView(
        physics: const BouncingScrollPhysics(),
        children: [
          SizedBox(
            height: 50,
            child: Center(


              child: Text(
                "Liked by $numberLikes users",
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(
                  fontSize: 18.0,
                  color: Helper.primaryColor,
                  letterSpacing: 1,
                ),
              ),

            ),

          ),
          getReviews(),
        ],
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          ElevatedButton(
            child: const Icon(Icons.add, color: Colors.white),
            style: Helper.kmandiButtonStyle(Helper.brownishColor),
            onPressed: () {
              showModalBottomSheet(
                barrierColor: Helper.brownishDarkColor.withOpacity(0.1),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
                context: context,
                builder: (context) => Container(
                  margin: const EdgeInsets.all(20),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: Form(
                          key: _keyForm,
                          child: Column(
                            children: [
                              TextFormField(
                                keyboardType: TextInputType.streetAddress,
                                decoration:
                                Helper.kmandiInputDecoration("Review"),
                                maxLines: 5,
                                onSaved: (String? value) {
                                  comment = value!;
                                },
                                validator: (value) {
                                  return Helper.addressValidator(value);
                                },
                              ),

                            ],
                          ),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () async {
                          SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                          token = prefs.getString("token")!;
                          var prodId = prefs.getString("productId");
                          var id = prefs.getString("id");
                          if (_keyForm.currentState!.validate()) {
                           _keyForm.currentState!.save();
                          Map<String, dynamic> reviewData = {
                          "Description": comment,
                            "User": id,
                            };
                           http.Response response = await http.post(Uri.http(_baseUrl,"/reviews/add"),body: json.encode(reviewData),headers: {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                            'Authorization': 'Bearer ${token}',
                          });
                            debugPrint(response.body.toString());
                            Map<String, dynamic> reviewServer = json.decode(response.body);
                            var reviewId = reviewServer["_id"];
                            Map<String, dynamic> productUpdate = {
                              "reviews": reviewId,
                            };
                            http.put(Uri.http(_baseUrl,"/products/"+prodId!),body: json.encode(productUpdate)
                                ,headers: {
                                  'Content-Type': 'application/json',
                                  'Accept': 'application/json',
                                  'Authorization': 'Bearer ${token}',
                                });
                        }},
                        child: const Icon(Icons.check, color: Colors.white),
                        style: Helper.kmandiButtonStyle(Helper.brownishColor),
                      ),

                    ],
                  ),
                ),
              );
            },
          ),
        ],
      ));
}

FutureBuilder<bool> getReviews() {
  return FutureBuilder(
    future: fetchedReviews,
    builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
      if (snapshot.hasData) {
        debugPrint("in getReviews");
        return reviewsList(context);
      } else {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
    },
  );
}

ListView reviewsList(BuildContext context) {
  return ListView.builder(
      shrinkWrap: true,
      //scrollDirection: Axis.horizontal,
      physics: const BouncingScrollPhysics(),
      itemCount: _reviews.length,
      itemBuilder: (BuildContext context, int index) => reviewCard(
        _reviews[index].username,
        _reviews[index].avatarImg,
        _reviews[index].commentaire,
        _reviews[index].date,
      ));
}

SizedBox reviewCard(
    String username,
    String avatarImg,
    String date,
    String commentaire,
    ) {
  return SizedBox(
    child: Card(
        color: Colors.white.withOpacity(0),
        elevation: 0,
        margin: const EdgeInsets.all(10),
        child: Column(
          children: [
            Row(
              children: [
                CircleAvatar(child: Image.network("http://"+_baseUrl +"/"+ avatarImg, headers: {
                  'Content-Type': 'application/json',
                  'Accept': 'application/json',
                  'Authorization': 'Bearer ${token}',
                },),),
                const SizedBox(
                  width: 5,
                ),
                Text(date),
              ],
            ),
            const SizedBox(
              height: 5,
            ),
            ReadMoreText(
              commentaire,
              trimLines: 2,
              colorClickableText: Helper.brownishColor,
              trimMode: TrimMode.Line,
              trimCollapsedText: 'Show more',
              trimExpandedText: 'Show less',
              moreStyle: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Helper.brownishColor),
              lessStyle: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Helper.brownishColor),
              style: const TextStyle(color: Helper.hintColor),
            ),
          ],
        )),
  );
}
}

class Review {
  final String username;
  final String avatarImg;
  final String date;
  final String commentaire;

  Review(this.username, this.avatarImg, this.date, this.commentaire);

  @override
  String toString() {
    return 'Review{username: $username, image: $avatarImg, date: $date, commentaire: $commentaire}';
  }
}
