import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'event_info.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Events extends StatefulWidget {
  const Events({Key? key}) : super(key: key);

  @override
  State<Events> createState() => _EventsState();
}

class _EventsState extends State<Events> {
  final List<Event> _Events = [];

  final String _baseUrl= "10.0.2.2:4000";

  late Future<bool> fetchedEvents;

  Future<bool> fetchEvents () async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token").toString();
    http.Response response =
    await http.get(Uri.http(_baseUrl, "/events"), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    debugPrint(response.body.toString());
    List<dynamic> eventsFromServer = json.decode(response.body);
    debugPrint(eventsFromServer[0].toString());

    debugPrint(
        double.parse(eventsFromServer[0]["price"].toString()).toString());
    for (int i = 0; i < eventsFromServer.length; i++) {
      Map<String, dynamic> eventFromServer = eventsFromServer[i];
      //List<String> imageName = eventFromServer["image"].split("\\");
      debugPrint(eventFromServer["_id"].toString());
      _Events.add(Event(
          eventFromServer["_id"],
          eventFromServer["image"],
          eventFromServer["title"],
          eventFromServer["description"],
          double.parse(eventFromServer["price"].toString()),
          token,
          eventFromServer["date"]
      ));
    }
    return true;
  }


  @override
  void initState() {
    fetchedEvents=fetchEvents();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchedEvents,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if(snapshot.hasData) {
          return ListView.builder(
            itemCount: _Events.length,
            itemBuilder: (BuildContext context,int index) {
              return EventInfo(_Events[index].image, _Events[index].title, _Events[index].description,
                  _Events[index].price,_Events[index].token,_Events[index].date);
            },
          );
        }
        else
        {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}