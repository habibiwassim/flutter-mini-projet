import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';

import 'event_details.dart';

class EventInfo extends StatelessWidget {
  final String _image;
  final String _title;
  //final String _reviews;
  final String _description;
  final double _price;
  final String _date;
  final String _token;
  EventInfo(this._image, this._title, this._description, this._price, this._token,this._date
      );

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext context) {
                return EventDetails(_image, _title, _description, _price, _token,_date);
              }));
        },
        child: eventCard(context, 2));
  }

  Container eventCard(BuildContext context, double sc) {
    return Container(
        margin: const EdgeInsets.all(60),
        child: Center(
          child: Column(
            children: [
              Transform.scale(
                  scale: sc,
                  child: Container(
                    width: 150,
                    height: 100,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    clipBehavior: Clip.antiAlias,
                    child: Image.network(
                      "http://10.0.2.2:4000/uploads/" + _image,
                      headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer $_token',
                      },
                      fit: BoxFit.fitWidth,
                    ),
                  )),
              const SizedBox(
                height: 60,
              ),
              Text(
                _title,
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: Helper.primaryColor, fontSize: 16),
              ),
              const SizedBox(
                height: 10,

              ),
              Text(
                _date,
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: Helper.primaryColor, fontSize: 16),
              ),
            ],
          ),
        ));
  }
}

class Event {
  final String id;
  final String image;
  final String title;
  final String date;
  final String description;
  final double price;
  final String token;

  Event(
      this.id, this.image, this.title, this.description, this.price, this.token,this.date);

  @override
  String toString() {
    return 'Event{id: $id, image: $image, title: $title, description: $description,price: $price,date: $date}';
  }
}
