import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:readmore/readmore.dart';

class EventDetails extends StatefulWidget {
  const EventDetails(
      this._image, this._title,this._description ,this._price, this._token,this._date,
      {Key? key})
      : super(key: key);

  final String _date;
  final String _description;
  final String _image;
  final String _title;
  final double _price;
  final String _token;

  @override
  State<EventDetails> createState() => _EventDetailsState();
}

class _EventDetailsState extends State<EventDetails> {
  final String _baseUrl = "10.0.2.2:4000";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget._title),
        backgroundColor: Colors.white.withOpacity(0),
        foregroundColor: Helper.brownishColor,
        shadowColor: Colors.white.withOpacity(0),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back_ios),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.shopping_cart_outlined),
            onPressed: () {/*TODO navigation au panier*/},
          ),
        ],
      ),
      body: ListView(
        children: [
          Container(
            width: double.infinity,
            margin: const EdgeInsets.all(20),
            child: Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: [
                Container(
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                  ),
                  clipBehavior: Clip.antiAlias,
                  child: Image.network(
                    "http://10.0.2.2:4000/uploads/" + widget._image,
                    headers: {
                      'Content-Type': 'application/json',
                      'Accept': 'application/json',
                      'Authorization': 'Bearer ${widget._token}',
                    },
                  ),
                ),
                Container(
                  //margin: const EdgeInsets.all(20),
                  //padding: const EdgeInsets.all(20),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.circular(20),
                    ),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  clipBehavior: Clip.antiAlias,
                  height: 100,
                  width: double.infinity,
                  child: ImageFiltered(
                      imageFilter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            gradient: LinearGradient(
                              begin: FractionalOffset.topCenter,
                              end: FractionalOffset.bottomCenter,
                              colors: [
                                Colors.grey.withOpacity(0.0),
                                Colors.black,
                              ],
                              stops: const [0.0, 1.0],
                            )),
                      )),
                ),
                Container(
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    dense: true,
                    title: Text(
                      widget._title,
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 22.0),
                    ),
                    subtitle: Text(
                      widget._title,
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                          fontSize: 16.0),
                    ),

                    //tileColor: Colors.black,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  "Description :",
                  textScaleFactor: 1.2,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 20,
                ),
                ReadMoreText(
                  "Flutter is Google’s mobile UI open source framework"
                      "to build high-quality native (super fast) interfaces "
                      "for iOS and Android apps with the unified codebase."
                      "Flutter is Google’s mobile UI open source framework"
                      "to build high-quality native (super fast) interfaces "
                      "for iOS and Android apps with the unified codebase."
                      "Flutter is Google’s mobile UI open source framework"
                      "to build high-quality native (super fast) interfaces "
                      "for iOS and Android apps with the unified codebase.",
                  trimLines: 2,
                  colorClickableText: Helper.brownishColor,
                  trimMode: TrimMode.Line,
                  trimCollapsedText: 'Show more',
                  trimExpandedText: 'Show less',
                  moreStyle: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Helper.brownishColor),
                  lessStyle: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Helper.brownishColor),
                  style: TextStyle(color: Helper.primaryColor),
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                widget._price.toString() + " TND",
                textScaleFactor: 3,
                textAlign: TextAlign.left,
              ),
              const SizedBox(
                width: 40,
              ),
              ElevatedButton(
                onPressed: () {/*TODO navigation au panier*/},
                child: Text("Book Now!!"),
                style: Helper.kmandiButtonStyle(Helper.brownishDarkColor),
              )
            ],
          ),
          //Text("Category : " + widget._category.toString()),
        ],
      ),
    );
  }
}
