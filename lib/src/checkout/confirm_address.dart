import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:positioned_tap_detector_2/positioned_tap_detector_2.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MapSample extends StatefulWidget {
  static const String route = '/tap';

  const MapSample({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MapSampleState();
  }
}

class MapSampleState extends State<MapSample> {
   LatLng tappedPoint = LatLng(45.5231, -122.6765);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tap to add pins')),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Text('Tap to choose location'),
            ),
            Flexible(
              child: FlutterMap(
                options: MapOptions(
                    center: LatLng(45.5231, -122.6765),
                    zoom: 13.0,
                    onTap: _handleTap
                ),
                layers: [
                  TileLayerOptions(
                    urlTemplate:
                    'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                  ),
                  MarkerLayerOptions(markers: [
                    Marker(
                    width: 80.0,
                    height: 80.0,
                    point: tappedPoint,
                    builder: (ctx) => Container(
                      child: Icon(Icons.location_on,color: Colors.red,size: 50,),
                    ),
                  ),
                  ])
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString("deliveryAddress", tappedPoint.toString());
          Navigator.pushReplacementNamed(context, "/Payement");
        },
        label: Text('confirm Address'),icon: Icon(Icons.thumb_up_rounded),),
    );
  }

  void _handleTap(TapPosition tapPosition, LatLng latlng)  {
    setState(()  {
      tappedPoint = latlng;

    });
  }
}