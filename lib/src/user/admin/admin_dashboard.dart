import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/user/admin/qr_create.dart';

class AdminDashboard extends StatelessWidget {
  const AdminDashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Admin Dashboard"),
      ),
      body: ListView(
        children: [
          const SizedBox(
            height: 20,
          ),
          ListTile(
            title: const Text(
              "Manage Admins",
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {},
          ),
          const SizedBox(
            height: 20,
          ),
          ListTile(
            title: const Text(
              "Manage Products",
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {},
          ),
          const SizedBox(
            height: 20,
          ),
          ListTile(
            title: const Text(
              "Create QR code for tables",
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                return const QrCreate();
              }));
            },
          ),
        ],
      ),
    );
  }
}
