import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:screenshot/screenshot.dart';

class QrCreate extends StatefulWidget {
  const QrCreate({Key? key}) : super(key: key);

  @override
  State<QrCreate> createState() => _QrCreateState();
}

class _QrCreateState extends State<QrCreate> {
  final textController = TextEditingController();
  final screenShotController = ScreenshotController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("QR Create"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            createdQR(),
            const SizedBox(
              height: 50,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: TextField(
                controller: textController,
                onChanged: (_) {
                  setState(() {
                    textController;
                  });
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(20, 80, 20, 00),
              child: ElevatedButton(
                child: SizedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        "Save QR Image",
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  width: 300,
                  height: 50,
                ),
                onPressed: () async {
                  final image =
                      await screenShotController.captureFromWidget(createdQR());
                  if (image != null) {
                    await saveImage(image);
                    showDialog(context: context, builder: (BuildContext context) {
                      return const AlertDialog(
                        title: Text("Informationn"),
                        content: Text("Image saved to Gallery  !!"),
                      );
                    });
                  }
                },
                style: Helper.kmandiButtonStyle(Helper.brownishColor),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget createdQR() {
    return Container(
      color: Colors.white,
      child: QrImage(
        data: textController.text,
        size: 300,
        gapless: false,
      ),
    );
  }

  Future<String> saveImage(Uint8List image) async {
    final time = DateTime.now()
        .toIso8601String()
        .replaceAll(".", "-")
        .replaceAll(":", "-");
    final name = "QR_Table_$time";
    final result = await ImageGallerySaver.saveImage(image, name: name);
    return result['filePath'];
  }
}
