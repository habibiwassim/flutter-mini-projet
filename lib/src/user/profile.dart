import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/helpers/helper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:shared_preferences/shared_preferences.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  late String? _userAvatarUrl;
  late String? _id;
  late String? _token;
  late String? _email;
  late String? _username;
  late String? _phonenumber;
  late String? _address;
  //late String? _password;
  //late String? _confirmPassword;
  TextEditingController _pass = TextEditingController();
  TextEditingController _confpass = TextEditingController();

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  final String _baseUrl = "10.0.2.2:4000";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Profile"),
          backgroundColor: Colors.white.withOpacity(0),
          foregroundColor: Helper.brownishColor,
          shadowColor: Colors.white.withOpacity(0),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back_ios),
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.shopping_cart_outlined),
              onPressed: () {/*TODO navigation au panier*/},
            ),
          ],
        ),
        body: Form(
            key: _keyForm,
            child: ListView(
              children: [
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                  child: CircleAvatar(
                    child: Image.asset("assets/images/logoFlutter.png"),
                  ),
                  height: 130,
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "Hello " "Username",
                  style: GoogleFonts.roboto(
                    fontSize: 18.0,
                    color: Helper.primaryColor,
                    letterSpacing: 1.6,
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 20,
                ),
                InkWell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const FaIcon(
                        FontAwesomeIcons.edit,
                        color: Helper.brownishColor,
                      ),
                      Text(
                        "Edit Avatar",
                        style: GoogleFonts.roboto(
                          fontSize: 16.0,
                          color: Helper.brownishColor,
                          letterSpacing: 1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  onTap: () {
                    //TODO edit avatar
                  },
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                  child: TextFormField(
                    keyboardType: TextInputType.name,
                    decoration: Helper.kmandiInputDecoration("Name"),
                    onSaved: (String? value) {
                      _username = value;
                    },
                    validator: (String? value) {
                      return Helper.nameValidator(value);
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: Helper.kmandiInputDecoration("Email Address"),
                    onSaved: (String? value) {
                      _email = value;
                    },
                    validator: (String? value) {
                      return Helper.emailValidator(value);
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                  child: TextFormField(
                    keyboardType: TextInputType.phone,
                    decoration: Helper.kmandiInputDecoration("Phone N°"),
                    onSaved: (String? value) {
                      _phonenumber = value;
                    },
                    validator: (String? value) {
                      return Helper.phoneValidator(value);
                    },
                  ),
                ),
                Container(
                    margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                    child: TextFormField(
                      keyboardType: TextInputType.streetAddress,
                      decoration: Helper.kmandiInputDecoration("Address"),
                      maxLines: 3,
                      onSaved: (String? value) {
                        _address = value;
                      },
                      validator: (value) {
                        return Helper.addressValidator(value);
                      },
                    )),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    decoration: Helper.kmandiInputDecoration("Mot de passe"),
                    //onSaved: (String? value) {
                    //  _password = value;
                    //},
                    controller: _pass,
                    validator: (String? value) {
                      return Helper.passwordValidator(value);
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 30, 20, 10),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    decoration: Helper.kmandiInputDecoration(
                        "Confirmation de mot de passe"),
                    //onSaved: (String? value) {
                    //  _confirmPassword = value;
                    //},
                    controller: _confpass,
                    validator: (String? value) {
                      return Helper.confpwValidator(value, _pass.text);
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 30, 20, 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        child: Text(
                          "Save",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                            fontSize: 16.0,
                            color: Colors.white70,
                            letterSpacing: 1.6,
                          ),
                        ),
                        onPressed: () async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          _token = prefs.getString("token");
                          _id = prefs.getString("id");
                          {
                            if (_keyForm.currentState!.validate()) {
                              _keyForm.currentState!.save();
                              Map<String, dynamic> userData = {
                                "fullName": _username,
                                "email": _email,
                                "password": _pass.text,
                                "address": _address,
                                "number": _phonenumber,
                              };
                              Map<String, String> headers = {
                                "Content-Type":
                                    "application/json; charset=UTF-8",
                                'Accept': 'application/json',
                                'Authorization': 'Bearer $_token',
                              };
                              http
                                  .put(Uri.http(_baseUrl, "/users/$_id"),
                                      headers: headers,
                                      body: json.encode(userData))
                                  .then((http.Response response) async {
                                if (response.statusCode == 200) {
                                  Map<String, dynamic> userFromServer =
                                      json.decode(response.body);
                                  prefs.setString(
                                      "token", userFromServer["token"]);
                                  print(prefs.getString("token"));
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("informations"),
                                        content: Text(
                                            "Mise à jour effectué avec succés !"),
                                      );
                                    },
                                  );
                                  Navigator.pushReplacementNamed(
                                      context, "/Bottomnav");
                                } else {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return const AlertDialog(
                                          title: Text("Informationn"),
                                          content: Text(
                                              "Une erreur s'est produite, réessayer plus tard !"),
                                        );
                                      });
                                }
                              });
                            }
                            //Navigator.pushReplacementNamed(context, "/Signin");
                          }
                        },
                        style: Helper.kmandiButtonStyle(Helper.brownishColor),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          _keyForm.currentState!.reset();
                        },
                        child: Text(
                          "Cancel",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                            fontSize: 16.0,
                            color: Helper.brownishColor,
                            letterSpacing: 1.6,
                          ),
                        ),
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      side: const BorderSide(
                                          color: Color(0xFFb7573c), width: 3))),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.white.withOpacity(0.5)),
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 20)
              ],
            )));
  }
}
