import 'package:flutter/material.dart';
import 'package:flutter_mini_proj/src/auth/verif_phone.dart';
import 'package:flutter_mini_proj/src/basket/basket.dart';
import 'package:flutter_mini_proj/src/checkout/confirm_address.dart';
import 'package:flutter_mini_proj/src/home/home.dart';
import 'package:flutter_mini_proj/src/auth/logout.dart';
import 'package:flutter_mini_proj/src/navigation/bottom_nav.dart';
import 'package:flutter_mini_proj/src/auth/signin.dart';
import 'package:flutter_mini_proj/src/auth/signup.dart';
import 'package:flutter_mini_proj/src/navigation/sliders/slideone.dart';
import 'package:flutter_mini_proj/src/navigation/sliders/slidethree.dart';
import 'package:flutter_mini_proj/src/navigation/sliders/slidetwo.dart';
import 'package:flutter_mini_proj/src/payement/credit-card.dart';
import 'package:flutter_mini_proj/src/payement/payement_methode.dart';
import 'package:flutter_mini_proj/src/user/admin/admin_dashboard.dart';
import 'package:flutter_mini_proj/src/user/profile.dart';

import 'src/auth/reset.dart';
import 'src/auth/auth.dart';
import 'src/events/event.dart';

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Kmandi App",
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      routes: kmandiRoutes,
      //home: const AdminDashboard(),
    );
  }

  Map<String, WidgetBuilder> get kmandiRoutes {
    return {
      "/": (BuildContext context) {
        return const Auth();
      },
      "/Signin": (BuildContext context) {
        return const Signin();
      },
      "/Signup": (BuildContext context) {
        return const Signup();
      },
      "/Slideone": (BuildContext context) {
        return const Slideone();
      },
      "/Slidetwo": (BuildContext context) {
        return const Slidetwo();
      },
      "/Slidethree": (BuildContext context) {
        return const Slidethree();
      },
      "/Logout": (BuildContext context) {
        return const Logout();
      },
      "/Bottomnav": (BuildContext context) {
        return const BottomNav();
      },
      "/Home": (BuildContext context) {
        return const Home();
      },
      "/Reset": (BuildContext context) {
        return const Reset();
      },
      "/Profile": (BuildContext context) {
        return const Profile();
      },
      "/Event": (BuildContext context) {
        return const Events();
      },
      "/Basket": (BuildContext context) {
        return const Basket();
      },
      "/Admin": (BuildContext context) {
        return const AdminDashboard();
      },
      "/VerifPhone": (BuildContext context) {
        return const VerifPhone();
      },
      "/MapSample": (BuildContext context) {
        return const MapSample();
      },
      "/Payement": (BuildContext context) {
        return const Payement();
      },
      "/CC": (BuildContext context) {
        return const CreditCard();
      },
    };
  }
}
